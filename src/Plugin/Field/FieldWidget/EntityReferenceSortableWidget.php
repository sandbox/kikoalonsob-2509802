<?php

/**
 * @file
 * Contains \Drupal\entity_reference_sortable\Plugin\field\widget\EntityReferenceSortableWidget.
 */

namespace Drupal\entity_reference_sortable\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'entity_reference_sortable' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_sortable",
 *   label = @Translation("Entity reference sortable"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class EntityReferenceSortableWidget extends OptionsSelectWidget {

	/**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
  	$element = parent::formElement($items, $delta, $element, $form, $form_state);
  	$element['#theme'] = 'entity_reference_sortable_widget_select';

		$element['#field_suffix'] = array(
			'#theme' => 'entity_reference_sortable_widget_list'
		);

		$element['#attached'] = array(
      'library' =>  array(
        'entity_reference_sortable/entity_reference_sortable'
      ),
    );

  	return $element;
  }

	/**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $summary[] = t('Default configuration');

    return $summary;
  }
}
